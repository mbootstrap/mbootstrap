Mini Boot-Strap Script
======================

TBD

Configuration
-------------

A YAML file configures the behaviour of the bootstrap script. All resources
needed are described within this file.

An example file would look like this:

~~~yaml
required_version: 0.1.1

resources:
  - name: r1
    src: git@gitlab.com:markushutzler/mbootstrap.git
    method: git
    branch: master
    ref: 0.0.1
    path: external/boot
    patches:
        - 0001-example.patch

  - name: r2
    src: http://url-to-file.io/package.tar.gz
    method: tar
    path: external/package
    sundirectory-filter: lib
    sha256sum: ba7eb1781c9fbbae178c4c6bad1c6eb08edab9a1496c64833d1715d022b38335
~~~

The file needs to be called one of the following names and is searched in the
order shown:

 - `.mbootstrap.yaml`
 - `mbootstrap.yaml`
 - `.mbs.yaml`
 - `mbs.yaml`

### Git

The above configuration would clone the git repository and checkout the
reference `0.0.1` into the path `external/boot`. As the branch is given the
fetch operation will only retrieve the master branch.

### Tar

Tar files are downloaded and extracted into a staging area. All tar resources
are validated to not contain an absolute path.

### Patching

Patches can be added to the bootstrap script by placing them in
`.mbootsrap-patches/` folder next to the configuration file. Each resource
defines what patch is used after fetch and before installing the sources.

Tar resources cannot be patched at this stage.

### Subdirectory Filter

The subdirectory filter variable is used to select a single folder of an
uncompressed tar file or a fetched git repository. Only this subfolder is
installed as the target path.

Using subdirectory-filter in Git resources will remove any history or git
configuration from the fetched resource.
